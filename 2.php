<?php

function createArrayWithRandomValues(){
    $array = [];
    for ($i=0; $i<20; $i++){
        $array[] = rand(0,10);
    }
    return $array;
}

$scored_balls = createArrayWithRandomValues();
$missed_balls = createArrayWithRandomValues();

for ($i=0; $i<count($scored_balls); $i++){
    print "Game № " . var_export($i+1, true) . "\n";
    if ($scored_balls[$i] > $missed_balls[$i]){
        print "|------ {$scored_balls[$i]} : {$missed_balls[$i]} ------|\n";
        print "You win this match!\n\n";
    } elseif ($scored_balls[$i] < $missed_balls[$i]){
        print "|------ {$scored_balls[$i]} : {$missed_balls[$i]} ------|\n";
        print "You lose this match!\n\n";
    } else {
        print "|------ {$scored_balls[$i]} : {$missed_balls[$i]} ------|\n";
        print "Draw!\n\n";
    }
}