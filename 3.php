<?php

print "Введите 1 строку:\n";
$string1 = trim(fgets(STDIN));
print "Введите 2 строку:\n";
$string2 = trim(fgets(STDIN));

if ($string1 === $string2){
    print "Вы ввели одинаковые строки\n";
} else {
    print "В строках: ". $string1 . " и " . $string2 . "\n";
    print "Символов совпадает: ".
        var_export(similar_text($string1, $string2), true);
}