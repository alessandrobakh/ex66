<?php

$array = [];
$difference = 2;

for($i=0; $i<10; $i++){
    $array[] = $i*$difference;
}

function isProgression($array, $difference)
{
    $progression_difference = [];
    for ($j=9; $j>0; $j--){
        $progression_difference[] = $array[$j]-$array[$j-1];
    }
    foreach ($progression_difference as $item){
        if ($item == $difference){
            return $difference;
        }
    }
    return NULL;
}
var_export(isProgression($array, $difference));